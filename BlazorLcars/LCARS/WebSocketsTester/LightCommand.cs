﻿using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using System;
using System.Collections.Generic;
using System.Text;

namespace WebSocketsTester
{
    public class LightCommand
    {
        public int id;
        public string type = "call_service";
        public string domain = "light";
        public string service;
        public ServiceData service_data;

        public LightCommand(int id, string service, string entity_id)
        {
           
            this.id = id;
            this.service = service;
            this.service_data = new ServiceData(entity_id);
        }
    }

     public class ServiceData
    {
        public string entity_id;

        internal ServiceData(string entity_id)
        {
            this.entity_id = entity_id;
        }
    }
}
