﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WebSocketsTester
{
    public class Subcription
    {
        public int id;
        public string type = "subscribe_events";
        public string event_type = "state_changed";

        public Subcription(int id)
        {
            this.id = id;
        }
   }
}
