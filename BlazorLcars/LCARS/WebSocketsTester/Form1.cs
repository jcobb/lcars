﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net.WebSockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using Newtonsoft.Json;


//rememebr to check streamwebsockets for sending large data

namespace WebSocketsTester
{
    public partial class Form1 : Form
    {

        CancellationToken ct = new CancellationToken();
        ClientWebSocket ws = new ClientWebSocket();

        int commandNum = 1;

        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Task t = ws.ConnectAsync(new Uri("wss://homeassistant.norcom.cobbnet.net/api/websocket"), ct);

            t.Wait(ct);

            ArraySegment<byte> buffer = new ArraySegment<byte>(new byte[4096]);

            WebSocketReceiveResult result = null;


            do
            {
                result = ws.ReceiveAsync(buffer, CancellationToken.None).Result;
                WriteText(result.Count.ToString());
                WriteText(Encoding.UTF8.GetString(buffer));
            }

            while (!result.EndOfMessage);
        }


        public async Task WriteText(string text)
        {
           // Task.Run(() => BeginInvoke((Action)(() => { textBox1.AppendText(text); textBox1.AppendText(Environment.NewLine); })));
            //return stuff;

            //var stuff2 = textBox1.Invoke((MethodInvoker)(() => {textBox1.AppendText(text); textBox1.AppendText(Environment.NewLine); }));
            //return stuff;

            textBox1.BeginInvoke(new Action(() => { textBox1.AppendText(text); textBox1.AppendText(Environment.NewLine);}));

            textBox1.AppendText(text);




            //textBox1.AppendText(text);
            //textBox1.AppendText(Environment.NewLine);
        }

        public void WriteText(string text, bool newline)
        {
            textBox1.AppendText(text);
            textBox1.AppendText("||||");
            if(newline)
                textBox1.AppendText(Environment.NewLine);
        }

        public void WriteText2(string text)
        {
            textBox2.AppendText(text);
            textBox2.AppendText(Environment.NewLine);


            lbCount.Text = textBox2.Text.Length.ToString();
        }

        private void button2_Click(object sender, EventArgs e)
        {

            if (ws.State == WebSocketState.Open)
            {

                string jsonAuth = @"{ ""type"": ""auth"", ""access_token"": ""eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiI4OGRmNGZiMmZmNGM0MzA3YjQ0YjI0OWYxNGQ4Nzk5YSIsImlhdCI6MTU5ODgzOTE2MiwiZXhwIjoxOTE0MTk5MTYyfQ.1aQ-3I38mmbs9kmOvCWk3YP_cfnGZcJC5yRac88aCeI""}";

                ArraySegment<byte> buffer = new ArraySegment<byte>(Encoding.UTF8.GetBytes(jsonAuth));

                ws.SendAsync(buffer, WebSocketMessageType.Text, true, CancellationToken.None);
           

                WebSocketReceiveResult result = null;

                ArraySegment<byte> rxbuffer = new ArraySegment<byte>(new byte[8000]);

                do
                {
                    result = ws.ReceiveAsync(rxbuffer, CancellationToken.None).Result;
                    WriteText(result.Count.ToString());
                    WriteText(Encoding.UTF8.GetString(buffer));
                }

                while (!result.EndOfMessage);
            }


        }

        private void btnLightOn_Click(object sender, EventArgs e)
        {
            textBox2.Clear();

            if (ws.State == WebSocketState.Open)
            {

                LightCommand lc = new LightCommand(commandNum, "turn_on", "light.fan_light");


                string json = JsonConvert.SerializeObject(lc);

                ArraySegment<byte> txbuffer = new ArraySegment<byte>(Encoding.UTF8.GetBytes(json));

                WebSocketReceiveResult result = null;
                ws.SendAsync(txbuffer, WebSocketMessageType.Text, true, CancellationToken.None);
                commandNum++;

                ArraySegment<byte> rxbuffer = new ArraySegment<byte>(new byte[8000]);

                do
                {
                    result = ws.ReceiveAsync(rxbuffer, CancellationToken.None).Result;
                    WriteText(result.Count.ToString());
                    WriteText(Encoding.UTF8.GetString(rxbuffer));
                }

                while (!result.EndOfMessage);
            }


        }

        private async void btnLightOff_Click(object sender, EventArgs e)
        {
            textBox2.Clear();

            if (ws.State == WebSocketState.Open)
            {

                WriteText("-------------------------------------------");

                LightCommand lc = new LightCommand(commandNum, "turn_off", "light.fan_light");
                string json = JsonConvert.SerializeObject(lc);

                ArraySegment<Byte> rxbuffer = new ArraySegment<byte>(new Byte[4096]);

                //ArraySegment<byte> cbuffer = WebSocket.CreateClientBuffer(4096, 4096);


       
                //int myCOunt = cbuffer.Count();


                WebSocketReceiveResult result;

                ArraySegment<byte> txbuffer = new ArraySegment<byte>(Encoding.UTF8.GetBytes(json));

             

                await ws.SendAsync(txbuffer , WebSocketMessageType.Text, true, CancellationToken.None);
                commandNum++;

                using (var ms = new MemoryStream())
                {
                    // result.Count is important because it tells you how many byts to get from he RX stream

                    do
                    {
                        result = await ws.ReceiveAsync(rxbuffer, CancellationToken.None);
                        ms.Write(rxbuffer.Array, rxbuffer.Offset, result.Count);

                        //var count = result.Count;
                    }
                    while (!result.EndOfMessage);

                    ms.Seek(0, SeekOrigin.Begin);

                    if (result.MessageType == WebSocketMessageType.Text)
                    {
                        using (var reader = new StreamReader(ms, Encoding.UTF8))
                        {
                            string text = reader.ReadToEnd();

                            WriteText(text);
                        }
                    }
                 }
            }
        }

        private void btnSub_Click(object sender, EventArgs e)
        {


            HandleMessages();
            



        }




        public async Task HandleMessages()
        {

            ClientWebSocket sws = new ClientWebSocket();

            Task t = sws.ConnectAsync(new Uri("wss://homeassistant.norcom.cobbnet.net/api/websocket"), ct);

            t.Wait(ct);


            if (sws.State == WebSocketState.Open)
            {

                string jsonAuth = @"{ ""type"": ""auth"", ""access_token"": ""eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiI4OGRmNGZiMmZmNGM0MzA3YjQ0YjI0OWYxNGQ4Nzk5YSIsImlhdCI6MTU5ODgzOTE2MiwiZXhwIjoxOTE0MTk5MTYyfQ.1aQ-3I38mmbs9kmOvCWk3YP_cfnGZcJC5yRac88aCeI""}";

                ArraySegment<byte> authbuffer = new ArraySegment<byte>(Encoding.UTF8.GetBytes(jsonAuth));


                ArraySegment<Byte> rxbufferauth = new ArraySegment<byte>(new Byte[4096]);

                await sws.SendAsync(authbuffer, WebSocketMessageType.Text, true, CancellationToken.None);

                WebSocketReceiveResult resulta = null;

                do
                {
                    resulta = sws.ReceiveAsync(rxbufferauth, CancellationToken.None).Result;
                    WriteText2(resulta.Count.ToString());
                    WriteText2(Encoding.UTF8.GetString(authbuffer));
                }

                while (!resulta.EndOfMessage);

                WriteText2("Authorized");


            }



            Subcription sub = new Subcription(500);



            string json = JsonConvert.SerializeObject(sub); 



            ArraySegment<byte> txbufferSub = new ArraySegment<byte>(Encoding.UTF8.GetBytes(json));



            await sws.SendAsync(txbufferSub, WebSocketMessageType.Text, true, CancellationToken.None);


            WebSocketReceiveResult resultsub;

            ArraySegment<byte> rxbuffersub = new ArraySegment<byte>(new byte[8000]);

            using (var ms = new MemoryStream())
            {
                // result.Count is important because it tells you how many byts to get from he RX stream

                do
                {
                    resultsub = await sws.ReceiveAsync(rxbuffersub, CancellationToken.None);
                    ms.Write(rxbuffersub.Array, rxbuffersub.Offset, resultsub.Count);

                    //var count = result.Count;
                }
                while (!resultsub.EndOfMessage);

                ms.Seek(0, SeekOrigin.Begin);

                if (resultsub.MessageType == WebSocketMessageType.Text)
                {
                    using (var reader = new StreamReader(ms, Encoding.UTF8))
                    {
                        string text = reader.ReadToEnd();

                        WriteText2(text);
                    }
                }
            }








            WriteText2("Listening");

            try
            {
                using (var ms = new MemoryStream())
                {
                    StreamReader reader = new StreamReader(ms, Encoding.UTF8);

                    ArraySegment<byte> messageBuffer = WebSocket.CreateClientBuffer(1024, 16);

                    int lstnCount = 1;

                    while (sws.State == WebSocketState.Open)
                    {
                        WriteText2(lstnCount.ToString());
                        lstnCount++;

                        Application.DoEvents();

                        WebSocketReceiveResult resultEvent;

                        do
                        {
                            resultEvent = await sws.ReceiveAsync(messageBuffer, CancellationToken.None);
                            ms.Write(messageBuffer.Array, messageBuffer.Offset, resultEvent.Count);
                        }
                        while (!resultEvent.EndOfMessage);

                        //if (result.MessageType == WebSocketMessageType.Text)
                        //{
                        //    var msgString = Encoding.UTF8.GetString(ms.ToArray());

                        //    var message = JsonConvert.DeserializeObject<Dictionary<String, String>>(msgString);

                        //    if (message["roomCode"].ToLower() == RoomCode.ToLower())
                        //    {
                        //       // Debug.Log("[WS] Got a message of type " + message["messageType"]);
                        //        // Message was intended for us!
                        //        switch (message["messageType"])
                        //        {
                        //            // handle messages here, unimportant to stackoverflow
                        //        }
                        //    }
                        //}



                        ms.Seek(0, SeekOrigin.Begin);
                        ms.Position = 0;

                        if (resultEvent.MessageType == WebSocketMessageType.Text)
                        {
                            //using (var reader = new StreamReader(ms, Encoding.UTF8))
                            //{
                                string text = reader.ReadToEnd();

                                WriteText2(text);
                           // }
                        }

                    
                    }

                    reader.Dispose();
                }
            }
            catch (Exception ex)
            {
                WriteText2(ex.Message);
               // Debug.Log("[WS] Tried to receive message while already reading one.");
            }
        }

        private async void button3_Click(object sender, EventArgs e)
        {

            WriteText("Button Clicked Started");
            Application.DoEvents();
            await WriteMoreStuff();
            Application.DoEvents();
            System.Threading.Thread.Sleep(1000);
            WriteText("Button Clicked Ended");
            Application.DoEvents();
        }

        private async Task WriteMoreStuff()
        {
            await WriteText("WriteMoreStuff Started");
            Application.DoEvents();
            await Task.Delay(5000);
            WriteText("WriteMoreStuff Ending");
            Application.DoEvents();
            await Task.Delay(5000);

        }
    }
}

//public class ChatHub : Hub
//{
//    public Task SendMessage(string user, string message)
//    {
//        return Clients.All.SendAsync("ReceiveMessage", user, message);
//    }
//}


//eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiI4OGRmNGZiMmZmNGM0MzA3YjQ0YjI0OWYxNGQ4Nzk5YSIsImlhdCI6MTU5ODgzOTE2MiwiZXhwIjoxOTE0MTk5MTYyfQ.1aQ-3I38mmbs9kmOvCWk3YP_cfnGZcJC5yRac88aCeI