﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;
using LCARS.Shared;

namespace LCARS
{
    public class DimmingButtonGroup : LCARSButtonGroup
    {
        public DimmingButtonGroup()
        {
            //this should be something other than LcarsEntityCommandButtons
           // LCARSButtons.AddRange(new List<LcarsEntityCommandButton>() { new LcarsEntityCommandButton("High", "High"), new LcarsEntityCommandButton("Med Hi", "Med Hi"), new LcarsEntityCommandButton("Medium", "Med"), new LcarsEntityCommandButton("Med Low", "Med Low"), new LcarsEntityCommandButton("Low", "Low")});
            this.GroupName = "Dim";
        }
    }
}
