﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HomeAssistant
{
    public class Service
    {
        private Service(string HAServiceName)
        {
            HomeAssistantValue = HAServiceName;
        }

        //public string Label { get; }
        public string HomeAssistantValue; // => Label.ToLower();

        public static Service TurnOn => new Service("Turn_On");
        public static Service TurnOff => new Service("Turn_Off");
        public static Service SetSpeed => new Service("Set_Speed");
        public static Service SetTemp => new Service("Set_Temperature");

    }


    public class Speed
    {
        private Speed(string label)
        {
            Label = label;
        }

        public string Label { get; }

        public string HomeAssistantValue => Label.ToLower();

        public static Speed High => new Speed("High");
        public static Speed Medium => new Speed("Medium");
        public static Speed Low => new Speed("Low");
        public static Speed Off => new Speed("Off");
    }




    public sealed class EntityState
    {
        //public string Label { get; }
        public string HAEntityState;


        private EntityState(string homeAssistantValue)
        {
            HAEntityState = homeAssistantValue.ToLower();
        }

        public static EntityState On => new EntityState("On");
        public static EntityState Off => new EntityState("Off");

    }
}