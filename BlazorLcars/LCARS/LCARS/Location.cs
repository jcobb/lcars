﻿using LCARS.Shared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LCARS
{
    public class Location
    {
        public int FloorNumber;
        public string RoomName;
        public string MainTitle;

        public List<Location> AdjacentLocations;
        public List<LCARSButtonGroup> LCARSButtonGroups;

        public bool IsLeaf => AdjacentLocations.Count == 0;

        public string FloorName => $"{FloorNumber.ToOrdinal()} Floor";

        public Location(string roomName, int floorNumber)
        {
            this.FloorNumber = floorNumber;
            this.RoomName = roomName;
            //this.MainTitle = "Room Controls".ToUpper();

            this.AdjacentLocations = GetLocations(this);
            this.LCARSButtonGroups = GetButtonGroups(this);
        }

        private List<LCARSButtonGroup> GetButtonGroups(Location location)
        {
            //make this use location
            List<LCARSButtonGroup> locationButtonGroups = new List<LCARSButtonGroup>();
            locationButtonGroups.Add(new RGBLightGroup("light.living_room_fan_light"));
            locationButtonGroups.Add(new FanGroup("fan.living_room_fan"));

            //LCARSButtonGroup.SelectMany(x => x.LCARSButtons).Select(x => x.EntityName == "")

            //todo: make this work for all the button groups
           // LCARSButtonGroup[0].LCARSButtons.ForEach(x => x.EntityName = "light.living_room_fan_light");

            //todo make this use an ENUM For HIGH LOW OFF ON ETC.
            //LCARSButtonGroup[0].LCARSButtons.OfType<LcarsEntityCommandButton>().Where(x => x.Label == "HIGH" || x.Label == "ON").ToList().ForEach(y => y.Command = "ON");  //sets button commands based on button names
            //LCARSButtonGroup[0].LCARSButtons.OfType<LcarsEntityCommandButton>().Where(x => x.Label == "OFF").ToList().ForEach(y => y.Command = "OFF");

            //LCARS.Data.LCARSCommand.Instance.EntityStateChanged += UpdateButtonState;  //register event handler


            return locationButtonGroups;
        }

        private List<Location> GetLocations(Location location)
        {
            return null;  //this is where adjacent locations will go
        }
    }
}
