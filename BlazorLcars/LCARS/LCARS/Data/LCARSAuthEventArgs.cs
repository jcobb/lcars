﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LCARS.Data
{
    public class LCARSAuthEventArgs : EventArgs
    {
        public readonly bool IsAuthorized;

        public LCARSAuthEventArgs(bool isAuthorized)
        {
            IsAuthorized = isAuthorized;
        }
    }

  
}
