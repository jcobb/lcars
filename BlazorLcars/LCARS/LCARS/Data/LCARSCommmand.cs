﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.WebSockets;
using System.Threading.Tasks;
using System.Threading;
using System.Text;
using System.IO;
using Newtonsoft.Json.Linq;
using System.Media;
using Microsoft.AspNetCore.Mvc.ModelBinding.Validation;
using Newtonsoft.Json;
using Microsoft.Extensions.Primitives;
using System.Runtime.InteropServices;
using System.Diagnostics;
using System.Timers;
using Microsoft.Extensions.Hosting;

namespace LCARS.Data
{


    public sealed class LCARSCommand
    {
        CancellationToken ct = new CancellationToken();
        public ClientWebSocket ws = new ClientWebSocket();

        private int CommandCount = 0;
        private Queue<string> sendQueue = new Queue<string>();

        private void OnMessageChanged(LCARSMessageEventArgs e)
        {
              EventHandler handler = MessageChanged;  //this is for the debug stream
              handler?.Invoke(this, e);
        }

        private void OnEntityStateChanged(LCARSEntityStateChangedEventArgs e)
        {
            EventHandler handler = EntityStateChanged;
            handler?.Invoke(this, e);
        }

        static TimeSpan initialTimerInterval = new TimeSpan(0, 0, 3);
        static TimeSpan timerInterval = new TimeSpan(0, 0, 10);

        System.Timers.Timer _watchdog = new System.Timers.Timer(initialTimerInterval.TotalMilliseconds);
    


        public event EventHandler MessageChanged;
        public event EventHandler EntityStateChanged;

        bool _connected;
        bool _authenticated;

       
        private static readonly Lazy<LCARSCommand> lazy = new Lazy<LCARSCommand>(() => new LCARSCommand());

      

        public static LCARSCommand Instance { get { return lazy.Value; } }

        private LCARSCommand()
        { 
            _watchdog.Elapsed += WatchdogElapsed;
            _watchdog.Start();
            //SetupCommandObjects();

            new System.Threading.Thread(new ThreadStart(StartRXStream)).Start();
            new System.Threading.Thread(new ThreadStart(StartTXStream)).Start();

            //rxThread.Start();
            //txThread.Start();

            Subscribe();  //start listening to events
        }

        private void WatchdogElapsed(object sender, ElapsedEventArgs e)
        {
            System.Timers.Timer timer = (System.Timers.Timer)sender;

            timer.Interval = timerInterval.TotalMilliseconds;

            //e.SignalTime  log ?

            if (ws.State == WebSocketState.Closed || ws.State == WebSocketState.None)
            {
                timer.Stop();
                Connect();
                timer.Start();
            }

            return;  
        }


        private async Task SetupCommandObjects()
        {
            //Connect();
            //StartRXStream();
            //Authenticate();
            //await Subscribe();
    


            //var rxThread = new System.Threading.Thread(new ThreadStart(StartRXStream));
            //var txThread = new System.Threading.Thread(new ThreadStart(StartTXStream));

            //rxThread.Start();
            //txThread.Start();

            //StartRXStream();
            //Debug.WriteLine("START RX STREAM CALLED");
            //var test = 1;
            //StartTXStream();
            //Debug.WriteLine("START TX STREAM CALLED");
            //var test2 = 2;
        }

        public bool Connected
        {
            get
            {
               return ws.State == WebSocketState.Open;
            }
            //private set
            //{
            //    _connected = value;
            //}
        }

        public bool Authenticated
        {
            get
            {
                return _authenticated;
            }
            private set
            {
                _authenticated = value;
                OnAuthenticatedChanged(new LCARSAuthEventArgs(_authenticated));
            }
        }

        private void OnAuthenticatedChanged(LCARSAuthEventArgs e)
        {
            EventHandler handler = AuthenticatedChanged;
            handler?.Invoke(this, e);
        }

        public event EventHandler AuthenticatedChanged;

        //protected virtual void OnAuthenticatedChanged(LCARSAuthEventArgs e)
        //{
        //    EventHandler handler = AuthenticatedChanged;
        //    handler?.Invoke(this, e);
        //}


        public void StartRXStream()
        {
            Debug.WriteLine("START RX");

            try
            {
                using (var ms = new MemoryStream())
                {

                    StreamReader reader = new StreamReader(ms, Encoding.UTF8);

                    while (true)
                    {
                        while (ws.State == WebSocketState.Open)
                        {
                           
                            WebSocketReceiveResult result;

                            //////do loop start
                            do
                            {
                                ArraySegment<byte> rxbuffer = new ArraySegment<byte>(new byte[1024]);
                                //ArraySegment<byte> rxbuffer = WebSocket.CreateClientBuffer(1024, 16);
                                result = ws.ReceiveAsync(rxbuffer, CancellationToken.None).Result;

                                //string str = System.Text.Encoding.Default.GetString(rxbuffer.Array, 0, result.Count);

                                ms.Write(rxbuffer.Array, rxbuffer.Offset, result.Count);
                            }

                            while (!result.EndOfMessage);
                            //////do loop end


                            //ms.Seek(e0, SeekOrigin..Begin);
                            ms.Position = 0;

                            if (result.MessageType == WebSocketMessageType.Text)
                            {
                                string text = reader.ReadToEnd();

                                JObject rxJSON = JObject.Parse(text);

                                //dynamic rxJSON = JArray.Parse(text);

                                OnMessageChanged(new LCARSMessageEventArgs(true, text));  //this is for the debug stream

                                //var stuff = auth["type"];
                                var messageType = (string)rxJSON["type"];
                                // var mystuf = stuff.Value;

                                if (messageType == "auth_ok")
                                {
                                    Authenticated = true;
                                }
                                else if (messageType == "event")
                                {

                                    try
                                    {
                                        string entityId = (string)rxJSON["event"]["data"]["entity_id"];
                                        string oldState = (string)rxJSON["event"]["data"]["old_state"]["state"] ?? string.Empty;
                                        string newState = (string)rxJSON["event"]["data"]["new_state"]["state"] ?? string.Empty;
                                        // Dictionary<string, string> newAttributes = rxJSON["event"]["data"]["new_state"]["attributes"].ToObject<Dictionary<string, string>>();
                                        // Dictionary<string, string> oldAttributes = rxJSON["event"]["data"]["old_state"]["attributes"].ToObject<Dictionary<string, string>>();

                                       JObject newAttributesJobject = (JObject)rxJSON["event"]["data"]["new_state"]["attributes"];
                                       JObject oldAttributesJobject = (JObject)rxJSON["event"]["data"]["old_state"]["attributes"];

                                 
                                       Dictionary<string, object> newAttributes = newAttributesJobject.Properties().Where(x => x.Value.Type != JTokenType.Array).ToDictionary(k => k.Name, v => v.Value.ToObject<object>());
                                       Dictionary<string, object> oldAttributes = oldAttributesJobject.Properties().Where(x => x.Value.Type != JTokenType.Array).ToDictionary(k => k.Name, v => v.Value.ToObject<object>());


                                       OnEntityStateChanged(new LCARSEntityStateChangedEventArgs(entityId, oldState, newState, oldAttributes, newAttributes));
                                    }
                                    catch (Exception ex)
                                    {
                                        var i = 0;
                                    }
                                }
                                else if (messageType == "result") //this is a home assitant result, not a WebSocketMessageType
                                {
                                    //dynamic entites = rxJSON["result"];

                                  

                                    JToken  entites = (JToken)rxJSON["result"];

                                    JTokenType resultType = entites.Type;
                                    
                                    //string rssTitle = rxJSON["result"];


                                    if (resultType == JTokenType.Array)
                                    {
                                        foreach (JObject entity in (JArray)entites)
                                        { 
                                            string entityId = (string)entity["entity_id"];
                                            string state = (string)entity["state"];


                                            JObject attributes = (JObject)entity["attributes"];

                                            Dictionary<string, object> newAttributes = new Dictionary<string, object>();
                                            Dictionary<string, object> oldAttributes = new Dictionary<string, object>();


                                            if (attributes != null)
                                            {
                                                newAttributes = attributes.Properties().Where(x => x.Value.Type != JTokenType.Array).ToDictionary(k => k.Name, v => v.Value.ToObject<object>());
                                            }


                                            if (!string.IsNullOrEmpty(entityId) && !string.IsNullOrEmpty(state) && newAttributes == null)
                                            {
                                                OnEntityStateChanged(new LCARSEntityStateChangedEventArgs(entityId, null, state));
                                            }
                                            else if (!string.IsNullOrEmpty(entityId) && !string.IsNullOrEmpty(state) && newAttributes != null)
                                            {
                                                OnEntityStateChanged(new LCARSEntityStateChangedEventArgs(entityId, null, state, oldAttributes, newAttributes));
                                            }
                                            
                                            
                                        }
                                    }
                                }
                                

                                ms.SetLength(0);  //set memory stream to zero length to clear it
                            }
                        }
                    }
                    reader.Dispose();
                }
            }
            catch (Exception ex)
            {
                string exc = ex.Message;
            }
        }

        public static bool HasProperty(dynamic obj, string name)
        {
            try
            {
                Type objType = obj.GetType();

                //if (objType == typeof(ExpandoObject))
                //{
                //    return ((IDictionary<string, object>)obj).ContainsKey(name);
                //}

                return objType.GetProperty(name) != null;
            }
            catch (Exception ex)
            {
                string error = ex.Message;
            }

            return false;
        }

        public void Subscribe()
        {
            CommandCount++;  //todo  command counter should be managed elsewhere
            SubscribeCommand sc = new SubscribeCommand(CommandCount);

            string json = JsonConvert.SerializeObject(sc, Newtonsoft.Json.Formatting.None);
            
            SendCommand(json);
        }


          

        public async void Connect()   //this is a call back to void is ok
        {
            try
            {
                Debug.WriteLine("CONNECT");

                if (ws.State == WebSocketState.Closed || ws.State == WebSocketState.None)
                { 
                    await   ws.ConnectAsync(new Uri("wss://homeassistant.norcom.cobbnet.net/api/websocket"), CancellationToken.None);  //if this crashes try creating a new WS when a reconnect is necessary

                    await Authenticate();

                    //if (ws.State == WebSocketState.Open)
                    //{
                    //    // Connected = true;
                    //    //WebSocketReceiveResult result;
                    //    // result = await ws.ReceiveAsync(rxbuffer, CancellationToken.None);
                    //}
                }
            }
            catch(Exception ex)
            {
                string message = ex.Message;
            }     
        }

        public async Task Authenticate()
        {
            Debug.WriteLine("Authenticate Running");

            if (ws.State == WebSocketState.Open)
            { 
                string jsonAuth = @"{ ""type"": ""auth"", ""access_token"": ""eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiI4OGRmNGZiMmZmNGM0MzA3YjQ0YjI0OWYxNGQ4Nzk5YSIsImlhdCI6MTU5ODgzOTE2MiwiZXhwIjoxOTE0MTk5MTYyfQ.1aQ-3I38mmbs9kmOvCWk3YP_cfnGZcJC5yRac88aCeI""}";

                ArraySegment<byte> buffer = new ArraySegment<byte>(Encoding.UTF8.GetBytes(jsonAuth));

                //https://stackoverflow.com/questions/23773407/a-websockets-receiveasync-method-does-not-await-the-entire-message

                Debug.WriteLine("Authenticate Sent");

                await ws.SendAsync(buffer, WebSocketMessageType.Text, true, CancellationToken.None);
            }
        }
        
        
        private void  StartTXStream()
        {
            Debug.WriteLine("START TX");

            try
            {
                while (true)
                {

                    if (Connected && Authenticated)
                    {
                        lock (sendQueue)
                        {
                            if (sendQueue.Any())
                            {
                                Debug.WriteLine("DEQUE START");


                                string json = sendQueue.Dequeue();
                                Debug.WriteLine("DEQUE END");
                                Debug.WriteLine($"Send command {json}");

                                ArraySegment<byte> txBuffer = new ArraySegment<byte>(Encoding.UTF8.GetBytes(json));
                                var str = System.Text.Encoding.Default.GetString(txBuffer);

                                OnMessageChanged(new LCARSMessageEventArgs(false, str));

                                Debug.WriteLine($"Command sent {str}");

                                ws.SendAsync(txBuffer, WebSocketMessageType.Text, true, CancellationToken.None);

                            }
                        }
                    }
                    //else  //timer takes care of this now
                    //{
                    //    if (!Connected)
                    //    {
                    //        Connect();
                    //    }

                    //    if (Connected && !Authenticated)
                    //    {
                    //        Authenticate();
                    //    }
                    //}
                }
            }
            catch(Exception ex)
            {
                string error = ex.Message;
            }
        }


        private void SendCommand(string json)
        {
            lock (sendQueue)
            {
                sendQueue.Enqueue(json);
            }
        }

        public void SetEntityState(string entityname, HomeAssistant.Service service)
        {
            CommandCount++;

            EntityCommand entityCommand = new EntityCommand(entityname, service, CommandCount);

            string json = JsonConvert.SerializeObject(entityCommand, Newtonsoft.Json.Formatting.None);

            SendCommand(json);
        }


        public void SetEntityState(string lightName, HomeAssistant.Service service, Dictionary<string, string> fields)
        {
            CommandCount++;

            EntityCommand lc = new EntityCommand(lightName, service, fields, CommandCount);

            string json = JsonConvert.SerializeObject(lc, Newtonsoft.Json.Formatting.None);

            SendCommand(json);
        }


        public void GetStates()
        {
            CommandCount++;

            LCARSCommandBase lc = new LCARSCommandBase();
            lc.type = "get_states";
            lc.id = CommandCount;

            string json = JsonConvert.SerializeObject(lc, Newtonsoft.Json.Formatting.None);

            SendCommand(json);
        }


        private class LCARSCommandBase
        {
            public int id;
            public string type;
        }


        private class EntityCommand : LCARSCommandBase
        {
            //public int id;

            public string domain;
            public string service;
            //public ServiceData service_data = new ServiceData();
            public Dictionary<string, string> service_data = new Dictionary<string, string>();

            public EntityCommand(string entityName, HomeAssistant.Service service, int commandId) : this(entityName, service , null, commandId)
            {
                //constructor
            }

            public EntityCommand(string entityName, HomeAssistant.Service serviceName,  Dictionary<string, string> fields , int commandId)
            {
                domain = entityName.Substring(0, entityName.IndexOf(".")).ToLower();
                this.id = commandId;
                type = "call_service";
                this.service = serviceName.HomeAssistantValue;

                this.service_data.Add("entity_id", entityName);


                if (fields != null)
                {
                    foreach (var field in fields)
                    {
                        service_data.Add(field.Key, field.Value);
                    }
                }
               
            }

            public class ServiceData
            {
                public string entity_id;
            }
        }

        private class SubscribeCommand : LCARSCommandBase
        {

            public string event_type = "state_changed";

            public SubscribeCommand(int commandId)
            {
                this.id = commandId;
                type = "subscribe_events";
            }
        }


    }
}



    //public class LCARSCommandHandler : System.Net.WebSockets.Handler
    //{

    //}


//https://www.codemag.com/Article/1210051/Real-Time-Web-Apps-Made-Easy-with-WebSockets-in-.NET-4.5


//https://fuqua.io/blog/2020/02/native-websockets-with-blazor-webassembly/
//closing code
//////if (receiveResult.MessageType == WebSocketMessageType.Close)
////{
////    // Bad.
////    //await socket.CloseAsync(WebSocketCloseStatus.NormalClosure, "", Token);

////    // Good.
////    await socket.CloseOutputAsync(WebSocketCloseStatus.NormalClosure, "", Token);
////}
////else
////{
////    // Read the buffer, do awesome stuff with the data.
////}
//////
///
///https://mcguirev10.com/2019/08/17/how-to-close-websocket-correctly.html
///



//using (var ms = new MemoryStream())
//{
//    // result.Count is important because it tells you how many byts to get from he RX stream

//    do
//    {
//        result = await ws.ReceiveAsync(rxbuffer, CancellationToken.None);
//        ms.Write(rxbuffer.Array, rxbuffer.Offset, result.Count);

//        //var count = result.Count;
//    }
//    while (!result.EndOfMessage);

//    ms.Seek(0, SeekOrigin.Begin);

//    if (result.MessageType == WebSocketMessageType.Text)
//    {
//        using (var reader = new StreamReader(ms, Encoding.UTF8))
//        {
//            string text = reader.ReadToEnd();

//            WriteText(text);
//        }
//    }
//}