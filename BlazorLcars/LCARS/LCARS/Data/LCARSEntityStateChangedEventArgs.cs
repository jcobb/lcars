﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LCARS.Data
{
    public class LCARSEntityStateChangedEventArgs : EventArgs
    {
        public string EntityId;
        public string OldState;
        public string NewState;

        public Dictionary<string, object> OldAttributes;
        public Dictionary<string, object> NewAttributes;

        public LCARSEntityStateChangedEventArgs(string entityId, string oldState, string newState, Dictionary<string, object> oldAttributes, Dictionary<string, object> newAttributes) //todo: replace with enumerations
        {
            EntityId = entityId;
            OldState = oldState;
            NewState = newState;
            OldAttributes = new Dictionary<string, object>(oldAttributes, StringComparer.OrdinalIgnoreCase);
            NewAttributes = new Dictionary<string, object>(newAttributes, StringComparer.OrdinalIgnoreCase);

        }

        public LCARSEntityStateChangedEventArgs(string entityId, string oldState, string newState) //todo: replace with enumerations
        {
            EntityId = entityId;
            OldState = oldState;
            NewState = newState;
        }

    }
}
