﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LCARS.Data
{
    public class LCARSMessageEventArgs : EventArgs
    {
        public readonly string Message;
        public readonly bool IsReceived;

        public LCARSMessageEventArgs(bool isReceived, string message)
        {
            IsReceived = isReceived;
            Message = message;
        }
    }
}
