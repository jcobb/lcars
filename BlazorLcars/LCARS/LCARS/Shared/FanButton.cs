﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Components.Web;

namespace LCARS.Shared
{
    public class FanButton : LcarsHACommandButton
    {
        public FanButton(string entityName, string label, HomeAssistant.EntityState entityState,  HomeAssistant.Service haService, string entityStateAttributeKey, string entityStateAttributeValue) : base(entityName, label, entityState, haService, entityStateAttributeKey, entityStateAttributeValue)
        {
           
            this.SendMessage += Test;
        }


        private void Test(object sender, MouseEventArgs e)
        {
            // bool on = EntityState == "ON";

            //HomeAssistant.Service service = this.EntityState.() == "ON" ? HomeAssistant.Service.TurnOn : HomeAssistant.Service.TurnOff;

           // HomeAssistant.Service service = EntityState.HomeAssistantValue == HomeAssistant.EntityState.On.HomeAssistantValue ? HomeAssistant.Service.TurnOn : HomeAssistant.Service.TurnOff;

            Dictionary<string, string> attributes = new Dictionary<string, string>() { [EntityStateAttributeKey] = EntityStateAttributeValue };


            LCARS.Data.LCARSCommand.Instance.SetEntityState(EntityName, HomeAssistant.Service.SetSpeed, attributes);

            LCARS.Data.LCARSCommand.Instance.SetEntityState(EntityName, HAService);
        }
    }
}
