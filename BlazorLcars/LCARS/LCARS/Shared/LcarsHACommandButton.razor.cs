﻿using Microsoft.AspNetCore.Components;
using Microsoft.AspNetCore.Components.Web;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Security.Authentication.ExtendedProtection;
using System.Threading.Tasks;

namespace LCARS.Shared
{
    public partial class LcarsHACommandButton : LCARS.Shared.LCARSButton
    {
        [Parameter]
        public string EntityName { get; set; }

        [Parameter]
        public string EntityStateAttributeValue { get; set; }  //button is on when this matches

        [Parameter]
        public string EntityStateAttributeKey { get; set; }

        [Parameter]
        public HomeAssistant.EntityState EntityState { get; set; }

        [Parameter]
        public HomeAssistant.Service HAService { get; set; }




        //[Parameter] 
        //public HomeAssistant.Service Service { get; set; }

        //public Action<LcarsEntityCommandButton> ProcessCommand { get; set; }


        //public LcarsEntityCommandButton(string label, string entityState) : base(label)
        //{
        //    EntityState = entityState;
        //}

        //public LcarsEntityCommandButton(string label, string entityState, string entityStateAttributeKey, string entityStateAttributeValue) : base(label)
        //{
        //    EntityStateAttributeKey = entityStateAttributeKey;
        //    EntityStateAttributeValue = entityStateAttributeValue;
        //    EntityState = entityState;
        //}


        public LcarsHACommandButton() : base(null)
        {

        }

        public LcarsHACommandButton(string entityName, string label,  HomeAssistant.EntityState entityState, HomeAssistant.Service haservice) : base(label)
        {
            EntityState = entityState;
            EntityName = entityName;
            HAService = haservice;

            //ProcessCommand = processCommand;

        }

        public LcarsHACommandButton(string entityName, string label,  HomeAssistant.EntityState entityState, HomeAssistant.Service haService, string entityStateAttributeKey, string entityStateAttributeValue) : this(entityName,  label,  entityState, haService)
        {
            EntityStateAttributeKey = entityStateAttributeKey;
            EntityStateAttributeValue = entityStateAttributeValue;
            //EntityState = entityState;
            //EntityName = entityName; 
            //ProcessCommand = processCommand;
        }

        public ButtonState GetState(string newState, Dictionary<string, object> newAttributes)  //todo replace this with an interface each button can use to return its own instance
        {
            object newStateAttribute = newAttributes != null && this.EntityStateAttributeKey != null && newAttributes.ContainsKey(this.EntityStateAttributeKey) ? newAttributes[this.EntityStateAttributeKey] : null;

            if (newState == this.EntityState.HAEntityState && this.EntityState.HAEntityState == "off")  //off states never have attributes
                return ButtonState.Active;

            if (newState == this.EntityState.HAEntityState && (newStateAttribute == null || this.EntityStateAttributeValue.ToUpper() == newStateAttribute.ToString().ToUpper()))
                return ButtonState.Active;

            return ButtonState.Inactive;

        }

        [Parameter]
        public EventHandler<MouseEventArgs> SendMessage { get; set; }


        public override async Task OnButtonClick(MouseEventArgs e)
        {
            SendMessage?.Invoke(this, e);

            ////ProcessCommand(this);  //<----------------  NRE HAPPENS HERE
            //bool on = EntityState == "ON";
            //LCARS.Data.LCARSCommand.Instance.SetEntityState(EntityName, on);
        }

        //public void ProcessCommand()  //<------------------------   DELEGATE
        //{
        //    bool on = EntityState == "ON" ? true : false;
        //    LCARS.Data.LCARSCommand.Instance.SetEntityState(EntityName, on);
        //}

        //ButtonState ILCARSAction.GetState(string state, Dictionary<string, string> entityState)
        //{
        //    throw new NotImplementedException();
        //}
    }

    

  
}