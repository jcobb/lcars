﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Components;

namespace LCARS.Shared
{
    public class LCARSControl : ComponentBase
    {
        private string _label;

        [Parameter]
        public string Label
        {
            get
            { return _label.ToUpper(); }
            set
            { _label = value; }
        }

        
    }
}
