﻿using System;
using Microsoft.AspNetCore.Components;
using Microsoft.AspNetCore.Components.Web;
using System.Threading.Tasks;
using System.Timers;
using Microsoft.JSInterop;

namespace LCARS.Shared
{
    public abstract partial class LCARSButton : LCARSControl
    {
       

        [Parameter]
        public ButtonState ButtonState { get; set; }

        public ButtonPressed ButtonPressed { get; set; } = ButtonPressed.NotPressed;

        public LCARSButton(string label)
        {
            Label = label;
            ButtonState = ButtonState.Inactive;  //default state
        }

        public abstract Task OnButtonClick(MouseEventArgs e);

        private async Task ProcessClick(MouseEventArgs arg)
        {
            FlashButtonAsync();
            BeepButtonAsync();
            await OnButtonClick(arg);
        }

        private async Task FlashButtonAsync()
        {
            await Task.Run(() =>
            {
                System.Timers.Timer flashTimer = new Timer(100)
                {
                    AutoReset = false
                };

                this.ButtonPressed = ButtonPressed.Pressed;


                flashTimer.Elapsed += (sender, args) => { this.ButtonPressed = ButtonPressed.NotPressed; };

                flashTimer.Start();
            });

            // haCommandButton.ButtonPressed = ButtonPressed.NotPressed;
        }

        private async Task BeepButtonAsync()
        {
            

            Random rnd = new Random();
            string clickNum = rnd.Next(1, 4).ToString();

            string randomKeyClick = $"keyok{clickNum}.mp3";
            js.InvokeVoidAsync("PlayAudioFile", $"/lcarsresources/sounds/keyclicks/{randomKeyClick}");
        }


        
    }

   


    public enum ButtonState
    {
        Active,
        Inactive,
        Disabled
    }


    public enum ButtonPressed
    {
        Pressed,
        NotPressed
    }
}
