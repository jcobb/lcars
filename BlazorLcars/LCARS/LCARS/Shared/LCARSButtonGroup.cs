﻿using Microsoft.AspNetCore.Mvc.ApiExplorer;
using Microsoft.AspNetCore.Mvc.ModelBinding.Validation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using System.Security.Cryptography.Xml;
using System.Threading.Tasks;
using LCARS.Shared;

namespace LCARS
{
    public class LCARSButtonGroup
    {
        private string _groupName;
        public string GroupName
        {
            get
            {
                return _groupName.ToUpper();
            }
            set
            {
                _groupName = value;
            }
        }
        
        public List<LcarsHACommandButton> LCARSButtons = new List<LcarsHACommandButton>();
        
        public LCARSButtonGroup()
        {
            LCARS.Data.LCARSCommand.Instance.EntityStateChanged += UpdateButtonState;  //register event handler, for when an state change API call comes in
        }


        public void UpdateButtonState(object sender, EventArgs e)   //this parses update messages, which contain all entities, looks for the specific entity in question and updates the button status
        {
            //TODO THIS NEEDS TO BE MOVED TO A HANDLER IN EACH BUTTON OR SOMETHING

            try
            {
                LCARS.Data.LCARSEntityStateChangedEventArgs args1 = (LCARS.Data.LCARSEntityStateChangedEventArgs)e;


                List<LcarsHACommandButton> affectedButtons = LCARSButtons.OfType<LcarsHACommandButton>().ToList().Where(y => y.EntityName == args1.EntityId).ToList();

                affectedButtons.ForEach(x => x.ButtonState = x.GetState(args1.NewState, args1.NewAttributes));  //todo make state get set by its component if possible

                //affectedButtons.Where(x => x.Command == (args1.OldState == null ? x.Command : args1.OldState.ToUpper())).ToList().ForEach(y => y.State = ButtonState.Inactive);
                //affectedButtons.Where(x => x.Command == args1.NewState.ToUpper()).ToList().ForEach(y => y.State = ButtonState.Active);

                if (buttonGroupStateChanged != null && affectedButtons.Any())
                {
                    buttonGroupStateChanged(sender, args1);  //should we call this event from the EntityStateChanges handler above?
                }

                //  InvokeAsync(() => StateHasChanged());
            }
            catch (Exception ex)
            {
                var i = 0;
            }

            

            //InvokeAsync(() => StateHasChanged());
        }

        public delegate void ButtonGroupStateChanged(object sender, LCARS.Data.LCARSEntityStateChangedEventArgs e);

        public event ButtonGroupStateChanged buttonGroupStateChanged;

    }
}