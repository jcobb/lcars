﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Components;
using Microsoft.AspNetCore.Components.Web;

namespace LCARS.Shared
{
    public class ClimateButton : LcarsHACommandButton
    {
        [Parameter] 
        public int TargetValue { get; set; }

        [Parameter]
        public Direction Direction { get; set; }

      

        public ClimateButton(string entityName, string label, HomeAssistant.EntityState entityState, HomeAssistant.Service haService, string entityStateAttributeKey, string currentTemp, LCARSDisplay lowTempDisplay, LCARSDisplay highTempDisplay) : base(entityName, label, entityState, haService, entityStateAttributeKey, currentTemp)
        {
            this.SendMessage += Test;
        }




        private void Test(object sender, MouseEventArgs e)
        {
            // bool on = EntityState == "ON";

            //HomeAssistant.Service service = this.EntityState.() == "ON" ? HomeAssistant.Service.TurnOn : HomeAssistant.Service.TurnOff;

            // HomeAssistant.Service service = EntityState.HomeAssistantValue == HomeAssistant.EntityState.On.HomeAssistantValue ? HomeAssistant.Service.TurnOn : HomeAssistant.Service.TurnOff;

            int newTemp = Convert.ToInt32(EntityStateAttributeValue);

            if (Direction == Direction.Decrease)
            {
                newTemp = newTemp - 1;
            }
            else if (Direction == Direction.Increase)
            {
                newTemp = newTemp + 1;
            }

            Dictionary<string, string> attributes = new Dictionary<string, string>() { [EntityStateAttributeKey] = newTemp.ToString() };

            //LCARS.Data.LCARSCommand.Instance.SetEntityState(EntityName, HomeAssistant.Service.SetTemp, attributes);

            LCARS.Data.LCARSCommand.Instance.SetEntityState(EntityName, HAService, attributes);
        }

    }
}
