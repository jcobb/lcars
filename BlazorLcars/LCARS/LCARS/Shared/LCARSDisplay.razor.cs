﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Components;
using Microsoft.Extensions.Primitives;

namespace LCARS.Shared
{
    public partial class LCARSDisplay : LcarsHACommandButton, IDisposable
    {
        //[Parameter]
        //public string Value { get; set; }


        //[Parameter]
        //public string EntityStateAttributeKey { get; set; }


        public void Dispose()
        {

            LCARS.Data.LCARSCommand.Instance.EntityStateChanged -= InstanceOnEntityStateChanged;

        }
    }
}
