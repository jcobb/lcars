﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Components.Web;

namespace LCARS.Shared
{
    public class LightButton : LcarsHACommandButton
    {
        
        public LightButton(string entityName, string label, HomeAssistant.EntityState entityState, HomeAssistant.Service haService) : base(entityName, label, entityState, haService)
        {
            this.SendMessage += Test;
        }

        private void Test(object sender, MouseEventArgs e)
        {
            //HomeAssistant.Service service = EntityState.HAEntityState == HomeAssistant.EntityState.On.HAEntityState ? HomeAssistant.Service.TurnOn : HomeAssistant.Service.TurnOff;


            LCARS.Data.LCARSCommand.Instance.SetEntityState(EntityName, HAService);
        }

        //public override void OnButtonClick(MouseEventArgs e)
        //{
        //    //ProcessCommand(this);  //<----------------  NRE HAPPENS HERE

        //}
    }
}
