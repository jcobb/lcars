﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LCARS
{
    public enum Direction
    {
        Decrease = 0,
        Increase = 1
    }
}
