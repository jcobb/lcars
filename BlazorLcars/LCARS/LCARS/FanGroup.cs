﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using LCARS.Shared;

namespace LCARS
{
    public class FanGroup : LCARSButtonGroup
    {
        private const string EntityStateAttributeKey = "speed";

        public FanGroup(string entityName)
        {
            LCARSButtons.AddRange(new List<LcarsHACommandButton>()
            {
                new FanButton(entityName, "High", HomeAssistant.EntityState.On, HomeAssistant.Service.TurnOn, EntityStateAttributeKey, "high"),
                new FanButton(entityName, "Medium", HomeAssistant.EntityState.On, HomeAssistant.Service.TurnOn,  EntityStateAttributeKey, "medium"),
                new FanButton(entityName, "Low",  HomeAssistant.EntityState.On, HomeAssistant.Service.TurnOn, EntityStateAttributeKey, "low"),
                new FanButton(entityName, "Off",  HomeAssistant.EntityState.Off, HomeAssistant.Service.TurnOff, EntityStateAttributeKey, "off")
            });

            //LCARSButtons.ForEach(x => x.EntityName = entityName);  //TODO move this to base class
            //LCARSButtons.ForEach(x => x.ProcessCommand = ProcessCommand);

            this.GroupName = "Fan";
        }

        //public void ProcessCommand(LcarsEntityCommandButton button)
        //{

        //    LCARS.Data.LCARSCommand.Instance.SetEntityState(button.EntityName, button.ServiceName);

        //    Dictionary<string, string> fields = new Dictionary<string, string>()
        //    {
        //        [button.EntityStateAttributeKey] = button.EntityStateAttributeValue
        //    };

        //  //  LCARS.Data.LCARSCommand.Instance.SetEntityState(button.EntityName, on, fields);
        //}
    }
}
