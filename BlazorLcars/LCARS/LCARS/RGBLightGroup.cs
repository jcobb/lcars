﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using LCARS.Shared;

namespace LCARS
{
    public class RGBLightGroup : LCARSButtonGroup
    {
        public RGBLightGroup(string entityName)
        {
            this.GroupName = "Lights";

            LCARSButtons.AddRange(new List<LcarsHACommandButton>() {
                new LightButton(entityName, "On", HomeAssistant.EntityState.On, HomeAssistant.Service.TurnOn),
               // new LightButton(entityName, "Medium", null),
              //  new LightButton(entityName, "Low", string.Empty),
                new LightButton(entityName, "Off", HomeAssistant.EntityState.Off, HomeAssistant.Service.TurnOff)
              //  new LcarsEntityCommandButton("Dim", string.Empty),  //todo this needs to change
              //  new LcarsEntityCommandButton("Hue", string.Empty)  //todo this needs to change
            });


            //foreach(LcarsEntityCommandButton button in LCARSButtons)
            //{
            //    System.Runtime.Serialization.ObjectIDGenerator idgen = new System.Runtime.Serialization.ObjectIDGenerator();

            //    bool known;

            //    long id = idgen.GetId(button, out known);
            //}

            //static void DoIt(LcarsEntityCommandButton button)
            //{
            //    bool on = button.EntityState == "ON" ? true : false;
            //    LCARS.Data.LCARSCommand.Instance.SetEntityState(button.EntityName, on);
            //}//<------------------------   DELEGATE


            //LCARSButtons.ForEach(x => x.EntityName = entityName);
           // LCARSButtons.ForEach(x => x.ProcessCommand = this.ProcessCommand);  //<-------------------------  ACTION IS SET HERE
        }

        //public void ProcessCommand(LcarsEntityCommandButton button)  //<------------------------   DELEGATE
        //{
        //    bool on = button.EntityState == "ON" ? true : false;
        //    LCARS.Data.LCARSCommand.Instance.SetEntityState(button.EntityName, on);
        //}
        
    }
}
