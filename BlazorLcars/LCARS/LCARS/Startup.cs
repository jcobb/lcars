using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Components;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using LCARS.Data;

namespace LCARS
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration; 
        }

        public IConfiguration Configuration { get; }


        // This method gets called by the runtime. Use this method to add services to the container.
        // For more information on how to configure your application, visit https://go.microsoft.com/fwlink/?LinkID=398940
        public void ConfigureServices(IServiceCollection services)
        {
            //not sure i need this, maybe for service side if we end up using it
            services.AddRazorPages();
            services.AddServerSideBlazor();
            //services.AddSingleton<WeatherForecastService>();
            //services.AddSingleton<LCARSCommand>();
            //services.AddSingleton<LCARSConfigService>();
            //services.Configure<LCARSConfigService>();


            //var builder = new ConfigurationBuilder();
           
        

            //IConfigurationSection sec = Configuration.GetSection("OrdersService");
            //services.Configure<OrderOptions>(sec);

            //ServiceProvider sp = services.BuildServiceProvider();
            //IOptions<OrderOptions> iop = sp.GetService<IOptions<OrderOptions>>();
            //OrderOptions op = iop.Value;

            //services.AddOrderService(options => {
            //    options.Value.OrderType = op.OrderType;
            //    options.Value.CutOffDate = op.CutOffDate;
            //});
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Error");
            }

            app.UseStaticFiles();

            app.UseRouting();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapBlazorHub();
                endpoints.MapFallbackToPage("/_Host");
            });
        }
    }
}
