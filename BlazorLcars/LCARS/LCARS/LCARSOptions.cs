﻿using System.Collections.Generic;

namespace LCARS
{
    public class LCARSOptions
    {
        public List<Location> Locations { get; internal set; }
    }
}